package com.sbfirst.demo.errors;

import java.util.Map;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

class SuperHeroAppErrorAttributes extends DefaultErrorAttributes {
	
    private final String currentApiVersion;
    private final String sendReportUri;

    public SuperHeroAppErrorAttributes(final String currentApiVersion, final String sendReportUri) {
        this.currentApiVersion = currentApiVersion;
        this.sendReportUri = sendReportUri;
    }

    //DK: Shows error while using <version>2.5.5</version> spring boot
    @Override
    public Map<String, Object> getErrorAttributes(final WebRequest webRequest, final boolean includeStackTrace) {
        final Map<String, Object> defaultErrorAttributes = super.getErrorAttributes(webRequest, false);
        final SuperHeroAppError superHeroAppError = SuperHeroAppError.fromDefaultAttributeMap(
                currentApiVersion, defaultErrorAttributes, sendReportUri
        );
        return superHeroAppError.toAttributeMap();
    }
}
