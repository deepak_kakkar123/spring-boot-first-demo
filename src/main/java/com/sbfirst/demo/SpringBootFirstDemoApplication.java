package com.sbfirst.demo;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.sbfirst.demo.model.DataSource1;
import com.sbfirst.demo.service.NotificationSender;
import com.sbfirst.demo.utils.ConfigurationOld;
import com.sbfirst.demo.utils.DentalConfiguration;
import com.sbfirst.demo.utils.HealthConfiguration;

@SpringBootApplication
@EnableConfigurationProperties
public class SpringBootFirstDemoApplication {

	public static void main(String[] args) {    
		SpringApplication.run(SpringBootFirstDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

			//Old Configurations
			System.out.println("Let's inspect the beans provided by Spring Boot:"); 
			ConfigurationOld c = (ConfigurationOld) ctx.getBean("configurationOld");
			System.out.println(c);
			//New Configurations
			HealthConfiguration c2 = (HealthConfiguration) ctx.getBean("healthConfiguration");
			System.out.println(c2);
			DentalConfiguration c3 = (DentalConfiguration) ctx.getBean("dentalConfiguration");
			System.out.println(c3);
			//DB Profiles
			//DataSource1 ds = (DataSource1) ctx.getBean("dataSource");
			//System.out.println(ds);
			
			//Notification, ConditionalProperty
			NotificationSender nSender1 = (NotificationSender) ctx.getBean("emailNotification");
			System.out.println(nSender1.send("hello"));
			//Here if sms property is set than only it will send the bean else exception of bean not dfines
			//NotificationSender nSender2 = ctx.getBean("smsNotification") == null ? null : (NotificationSender) ctx.getBean("smsNotification");
			//if(nSender2 != null)
				//System.out.println(nSender2.send("hello"));
			
			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				//System.out.println(beanName);
			}

		};
	}
}
