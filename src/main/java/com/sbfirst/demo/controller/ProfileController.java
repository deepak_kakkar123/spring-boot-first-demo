package com.sbfirst.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfileController {

	@Value("${spring.profiles.active}")
	private String activeProfile;
	@Value("${app.name}")
	private String appName; //common props
	@Value("${app.id}")
	private Integer appId; //common props
	@Value("${app.jar.path}")
	private String appJarPath; //specific pros
	@Value("${plan.service.url}")
	private String planServiceUrl;
	
	public ProfileController() {
		System.out.println("ProfileController instance created");
	}
	
	@RequestMapping("/profiles")
	public String profiles() {
		return "ProfileController [activeProfile=" + activeProfile + ", appName=" + appName + ", appId=" + appId
				+ ", appJarPath=" + appJarPath + ", planServiceUrl=" + planServiceUrl + "]";
	}

	
}
