package com.sbfirst.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sbfirst.demo.exceptions.CustomDataNotFoundException;
import com.sbfirst.demo.exceptions.CustomErrorException;
import com.sbfirst.demo.exceptions.CustomParameterConstraintException;

//@RestController
@Controller
public class HomeController1 {

	public HomeController1() {
		System.out.println("HomeController instance created");
	}

	@GetMapping("/home")
	public String index() {
		String s = null;
		s.length();
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/home1")
	public String index1() throws Exception {
		String s = null;
		if (s == null)
			throw new Exception("s i snull exception");
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/home3")
	public String index2() throws Exception {
		String s = null;
		if (s == null)
			throw new NullPointerException("s i snull exception");
		return "Greetings from Spring Boot!";
	}

	@GetMapping("test-custom-data-not-found-exception")
	public ResponseEntity<Void> test1() {
		try {
			// simulating a NullPointerException error
			throw new NullPointerException("Data not found");
		} catch (NullPointerException e) {
			throw new CustomDataNotFoundException(e.getMessage());
		}
	}

	@GetMapping("test-custom-parameter-constraint-exception")
	public ResponseEntity<Void> test2(@RequestParam("value") int value) {
		if (value < 0 || value > 10) {
			throw new CustomParameterConstraintException("value must be >= 0 and <= 10");
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("test-custom-error-exception")
	public ResponseEntity<Void> test3() {
		// simulating a CustomDataNotFoundException error
		throw new CustomErrorException(HttpStatus.BAD_REQUEST, "Parameters not passed");
	}

	@GetMapping("test-generic-exception")
	public ResponseEntity<Void> test4() {
		// simulating a generic error
		throw new RuntimeException("Generic Exception");
	}

}
