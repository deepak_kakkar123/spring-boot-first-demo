package com.sbfirst.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
	public LoginController() {
		System.out.println("LoginController instance created");
	}
	
	//@RequestMapping("/")
	public String home() {
		return "home.jsp";
	}
	
}
