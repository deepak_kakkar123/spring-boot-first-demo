package com.sbfirst.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sbfirst.demo.model.SuperHero;
import com.sbfirst.demo.repository.SuperHeroRepository;

@RestController
@RequestMapping("/superheroes")
public class SuperHeroController {

	private final SuperHeroRepository superHeroRepository;

	@Autowired
	public SuperHeroController(SuperHeroRepository superHeroRepository) {
		this.superHeroRepository = superHeroRepository;
	}

	@GetMapping("/{id}")
	public SuperHero getSuperHeroById(@PathVariable int id) {
		return superHeroRepository.getSuperHero(id);
	}

	@GetMapping
	public Optional<SuperHero> getSuperHeroByHeroName(@RequestParam("name") String heroName) {
		return superHeroRepository.getSuperHero(heroName);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void addNewSuperHero(@RequestBody SuperHero superHero) { //DK: commented @Valid
		superHeroRepository.saveSuperHero(superHero);
	}
}
