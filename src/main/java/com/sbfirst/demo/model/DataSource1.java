package com.sbfirst.demo.model;


// java.lang.ClassCastException: com.zaxxer.hikari.HikariDataSource cannot be cast to com.sbfirst.demo.model.DataSource
//So renamed
public class DataSource1 {

	private String username;
	private String password;
	private String host;
	private String port;
	
	public DataSource1() {
	}
	
	public DataSource1(String username, String password, String host, String port) {
		this.username = username;
		this.password = password;
		this.host = host;
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "DataSource [username=" + username + ", password=" + password + ", host=" + host + ", port=" + port
				+ "]";
	}
	
}
