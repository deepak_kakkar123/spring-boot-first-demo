package com.sbfirst.demo.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

//@Component
public class DataSourceConfig1 {

	@Bean(name ="dataSource")
	@Profile(value = "dev")
	DataSource1 getDevDataSource() {
		return new DataSource1("devu", "devp", "dev.host", "dev.port");
	}
	
	@Bean(name ="dataSource")
	@Profile(value = "qa")
	DataSource1 getQaDataSource() {
		return new DataSource1("qau", "qap", "qa.host", "qa.port");
	}
	
	@Bean(name ="dataSource")
	@Profile(value = "prod")
	DataSource1 getProdDataSource() {
		return new DataSource1("produ", "prodp", "prod.host", "prod.port");
	}
}
