package com.sbfirst.demo.service;

public interface NotificationSender {
	
	String send(String message);
	
}
