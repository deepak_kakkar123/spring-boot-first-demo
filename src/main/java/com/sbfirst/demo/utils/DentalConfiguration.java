package com.sbfirst.demo.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "dental")
public class DentalConfiguration {
	
	private String planName;
	private int planCount;
	
	
	public DentalConfiguration() {
		System.out.println("DentalConfiguration instance created");
	}


	public String getPlanName() {
		return planName;
	}


	public void setPlanName(String planName) {
		this.planName = planName;
	}


	public int getPlanCount() {
		return planCount;
	}


	public void setPlanCount(int planCount) {
		this.planCount = planCount;
	}


	@Override
	public String toString() {
		return "DentalConfiguration [planName=" + planName + ", planCount=" + planCount + "]";
	}

	
}
