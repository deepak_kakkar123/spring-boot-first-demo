package com.sbfirst.demo.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationOld {
	
	@Value("${health.plan-name}")
	private String healthPlanName;
	@Value("${dental.plan-name}")
	private String dentalPlanName;
	@Value("${health.plan-count}")
	private int healthPlanCount;
	@Value("${dental.plan-count}")
	private int dentalPlanCount;
	
	public ConfigurationOld() {
		System.out.println("Configuration instance created");
	}
	
	public String getHealthPlanName() {
		return healthPlanName;
	}
	public void setHealthPlanName(String healthPlanName) {
		this.healthPlanName = healthPlanName;
	}
	public String getDentalPlanName() {
		return dentalPlanName;
	}
	public void setDentalPlanName(String dentalPlanName) {
		this.dentalPlanName = dentalPlanName;
	}
	public int getHealthPlanCount() {
		return healthPlanCount;
	}
	public void setHealthPlanCount(int healthPlanCount) {
		this.healthPlanCount = healthPlanCount;
	}
	public int getDentalPlanCount() {
		return dentalPlanCount;
	}
	public void setDentalPlanCount(int dentalPlanCount) {
		this.dentalPlanCount = dentalPlanCount;
	}

	@Override
	public String toString() {
		return "Configuration [healthPlanName=" + healthPlanName + ", dentalPlanName=" + dentalPlanName
				+ ", healthPlanCount=" + healthPlanCount + ", dentalPlanCount=" + dentalPlanCount + "]";
	}

}
