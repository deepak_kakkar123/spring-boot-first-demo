package com.sbfirst.demo.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "health")
public class HealthConfiguration {
	
	private String planName;
	private int planCount;
	
	public HealthConfiguration() {
		System.out.println("HealthConfiguration instance created");
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public int getPlanCount() {
		return planCount;
	}

	public void setPlanCount(int planCount) {
		this.planCount = planCount;
	}

	@Override
	public String toString() {
		return "HealthConfiguration [planName=" + planName + ", planCount=" + planCount + "]";
	}

}
