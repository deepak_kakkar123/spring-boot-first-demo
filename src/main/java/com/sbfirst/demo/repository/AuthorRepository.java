package com.sbfirst.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.sbfirst.demo.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
